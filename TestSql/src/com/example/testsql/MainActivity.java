package com.example.testsql;

import com.andware.datebaseDeal.ICreateTable;
import com.andware.datebaseDeal.ICreateTableDemo;
import com.andware.datebaseDeal.SQLHelper;

import android.os.Bundle;
import android.app.Activity;
import android.view.Menu;

public class MainActivity extends Activity {

	private ICreateTable iCreateTable;

	private SQLHelper sqlHelper;
	
	@Override
	protected void onCreate(Bundle savedInstanceState) {
		super.onCreate(savedInstanceState);
		
		setContentView(R.layout.activity_main);
		sqlHelper = SQLHelper.getInstance(this);
		
		iCreateTable = new MyIcreatTableDemo();
		iCreateTable.createTables(sqlHelper.getWritableDatabase());
		
		
	}

	@Override
	public boolean onCreateOptionsMenu(Menu menu) {
		// Inflate the menu; this adds items to the action bar if it is present.
		getMenuInflater().inflate(R.menu.main, menu);
		return true;
	}

}
