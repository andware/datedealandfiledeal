package com.example.testsql;

import android.database.sqlite.SQLiteDatabase;
import android.util.Log;

import com.andware.datebaseDeal.ICreateTableDemo;
import com.andware.datebaseDeal.SqlScourceDeal;
import com.andware.module.Demo;

public class MyIcreatTableDemo extends ICreateTableDemo{

	@Override
	public void createTables(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		super.createTables(db);
		Log.i("Table", SqlScourceDeal.genCreateTableSql(Demo.class, "demo_table"));
		db.execSQL(SqlScourceDeal.genCreateTableSql(Demo.class, "demo_table"));
	}
	
}
