package com.andware.datebaseDeal;

import android.database.sqlite.SQLiteDatabase;

public interface ICreateTable {

	public void createTables ( SQLiteDatabase db );
	
}
