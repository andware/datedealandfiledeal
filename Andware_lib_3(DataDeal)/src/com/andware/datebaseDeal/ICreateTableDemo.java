package com.andware.datebaseDeal;

/**
 * @author ssj
 * 实现数据库表创建接口
 * 
 * **/

import android.database.sqlite.SQLiteDatabase;

public class ICreateTableDemo implements ICreateTable{

	private Class<?> myClass;
	
	private String table_name;
	
	public ICreateTableDemo ( Class<?> myClass , String table_name ) {
		this.myClass = myClass;
		this.table_name = table_name;
	}
	
	@Override
	public void createTables(SQLiteDatabase db) {
		// TODO Auto-generated method stub
		//for example、
		db.execSQL(SqlScourceDeal.genCreateTableSql(myClass, table_name));
	}

}
