package com.andware.datebaseDeal;

/**
 * 
 * Android 数据持久层封装
 * 
 * @author ssj
 * 
 * **/

import java.lang.reflect.Field;
import java.lang.reflect.InvocationTargetException;
import java.lang.reflect.Method;
import java.sql.Date;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.List;
import java.util.Map;

import android.R.integer;
import android.database.Cursor;
import android.util.Log;

public class SqlScourceDeal {
	
	
	
	/**
	 * 获取sql和params
	 * 
	 * @author ssj
	 * @param bean 需要生成插入语句的对象
	 * @param table 表名
	 * @return
	 * 
	 */
	public static Map getInsertSql(Object bean, String table) {
		String sql = "INSERT INTO " + table + " (";
		String values = "";
		// params是插入的参数值，用于PreparedStatement.set...
		ArrayList params = new ArrayList();
		Map map = new HashMap();

		// 取出bean里的所有字段
		Class beanClass = bean.getClass();
		Field[] fields = beanClass.getDeclaredFields();

		// 将map里的值赋给bean
		try {
			for (int i = 0; i < fields.length; i++) {
				String fieldname = fields[i].getName();
				if (i == 0)
					sql += fieldname;
				else
					sql += ", " + fieldname;

				// 调用get方法获取变量值
				Class fieldtype = fields[i].getType();
				String methodname = getMethodName(fieldname, "get");
				if ( methodname.equals("getId") ) {
					params.add(null);
				} else {
					Method method = beanClass.getMethod(methodname);

					Object fieldvalue = method.invoke(bean);


					params.add(fieldvalue);
				}
				if (i == 0)
					values += "?";
				else
					values += ", ?";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sql += ") VALUES(" + values + ")";

		map.put("sql", sql);
		map.put("params", params);
		return map;
	}

	/**
	 * 获取sql和params
	 * 
	 * @author ssj
	 * @param bean 需要生成插入语句的对象
	 * @param table 表名
	 * @return
	 * 
	 */
	public static Map getUpdateSql(Object bean, String table) {

		String sql = "REPLACE INTO " + table + " (";
		String values = "";
		// params是插入的参数值，用于PreparedStatement.set...
		ArrayList params = new ArrayList();
		Map map = new HashMap();

		// 取出bean里的所有字段
		Class beanClass = bean.getClass();
		Field[] fields = beanClass.getDeclaredFields();

		// 将map里的值赋给bean
		try {
			for (int i = 0; i < fields.length; i++) {
				String fieldname = fields[i].getName();
				if (i == 0)
					sql += fieldname;
				else
					sql += ", " + fieldname;

				// 调用get方法获取变量值
				Class fieldtype = fields[i].getType();
				String methodname = getMethodName(fieldname, "get");

				Method method = beanClass.getMethod(methodname);

				Object fieldvalue = method.invoke(bean);


				params.add(fieldvalue);
				if (i == 0)
					values += "?";
				else
					values += ", ?";
			}
		} catch (Exception e) {
			e.printStackTrace();
		}
		sql += ") VALUES(" + values + ")";

		map.put("sql", sql);
		map.put("params", params);
		return map;

	}

	/**
	 * 把cursor中的数据按照对象的属性取出来并set到对象当中并返回
	 * @param <E>
	 * @param req
	 * @param e
	 * @return <E>
	 */
	public static <E>E setParameter2JavaBean(Cursor cursor, Class<?> clazz){

		E e = null;
		try {
			e = (E) clazz.newInstance();
		} catch (InstantiationException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		} catch (IllegalAccessException e1) {
			// TODO Auto-generated catch block
			e1.printStackTrace();
		}

		Field[] fields = clazz.getDeclaredFields();
		int i = 0;

		for(Field field : fields){
			Class<?> typeClass = field.getType();
			Object obj = getClassTypeValueByCursor(typeClass,cursor,i);
			String value;
			value = String.valueOf(obj);
			if(value != null && !"".equals(value)){
				setValue(e, clazz, field, typeClass, value);
			}
			i++;
		}
		return e;
	}

	/**
	 * 调用set方法把值set到对象当中
	 * @param obj 指定对象
	 * @param clazz 对象的class
	 * @param field 需要设置值的field对象
	 * @param typeClass field的类型的class
	 * @param value 对应的值
	 */
	private static void setValue(Object obj, Class<?> clazz, Field field, Class<?> typeClass, String value){
		String fieldName = field.getName();
		String methodName = "set" + fieldName.substring(0, 1).toUpperCase() + fieldName.substring(1);
		try {
			Method method = clazz.getDeclaredMethod(methodName, new Class[]{typeClass});
			method.invoke(obj, new Object[]{getClassTypeValue(typeClass, value)});
		} catch (SecurityException e) {
			e.printStackTrace();
		} catch (IllegalArgumentException e) {
			e.printStackTrace();
		} catch (NoSuchMethodException e) {
			e.printStackTrace();
		} catch (IllegalAccessException e) {
			e.printStackTrace();
		} catch (InvocationTargetException e) {
			e.printStackTrace();
		}
	}

	/**
	 * 通过class类型获取获取对应类型的值
	 * @param typeClass class类型
	 * @param value 值
	 * @return Object
	 */
	private static Object getClassTypeValue(Class<?> typeClass, String value){
		if(typeClass == int.class){
			if(value.equals("")){
				return 0;
			}
			return Integer.parseInt(value);
		}else if(typeClass == short.class){
			if(value.equals("")){
				return 0;
			}
			return Short.parseShort(value);
		}else if(typeClass == byte.class){
			if(value.equals("")){
				return 0;
			}
			return Byte.parseByte(value);
		}else if(typeClass == double.class){
			if(value.equals("")){
				return 0;
			}
			return Double.parseDouble(value);
		}else if(typeClass == boolean.class){
			if(value.equals("")){
				return false;
			}
			return Boolean.parseBoolean(value);
		}else if(typeClass == float.class){
			if(value.equals("")){
				return 0;
			}
			return Float.parseFloat(value);
		}else if(typeClass == long.class){
			if(value.equals("")){
				return 0;
			}
			return Long.parseLong(value);
		}else {
			return typeClass.cast(value);
		}
	}

	/**
	 * 通过class类型获取获取对应类型的值
	 * @param typeClass class类型
	 * @param value 值
	 * @return Object
	 */
	private static Object getClassTypeValueByCursor( Class<?> typeClass, Cursor cursor,int arg0 ){
		if(typeClass.toString().equals("int")){
			return cursor.getInt(arg0);
		}else if(typeClass.toString().equals("short")){
			return cursor.getShort(arg0);
		}else if(typeClass.toString().equals("double")){
			return cursor.getDouble(arg0);
		}else if(typeClass.toString().equals("boolean")){
			return cursor.getInt(arg0)>0;
		}else if(typeClass.toString().equals("float")){
			return cursor.getFloat(arg0);
		}else if(typeClass.toString().equals("long")){
			return cursor.getLong(arg0);
		}else {
			return cursor.getString(arg0);
		}
	}

	/**
	 * 生成建表语句（目前只支持一对多,2级结构）
	 * 
	 * @author ssj
	 * 
	 * @param bean 需要创建table的类
	 * @param tableName 需要创建的表名
	 * @return 创建表的sql语句
	 */
	public static <T> String genCreateTableSql(Class<T> bean, String tableName) {
		boolean hasExters = false;
		List<String> beanPropertyList = getBeanPropertyList(bean);
		StringBuffer sb = new StringBuffer("CREATE TABLE IF NOT EXISTS " + tableName + " (\n");
		StringBuffer subSb = new StringBuffer( "CREATE TABLE IF NOT EXISTS ");
		for (String string : beanPropertyList) {
			String[] propertys = string.split("`");
			if (!propertys[1].equals("tableName")
					&& !propertys[1].equals("param")
					&& !propertys[0].equals("List")) {
				if (propertys[1].equals("id")) {

					sb.append("   id INTEGER primary key AUTOINCREMENT,\n");
				} else {
					if (propertys[0].equals("int")) {
						sb.append("   " + propertys[1] + " int default 0,\n");
					} else if (propertys[0].equals("String")) {
						sb.append("   " + propertys[1]
								+ " varchar(2000) default '',\n");
					} else if (propertys[0].equals("double")) {
						sb.append("   " + propertys[1]
								+ " double(10,2) default 0.0,\n");
					} else if ( propertys[0].equals("float") ) {
						sb.append("   " + propertys[1]
								+ " double(10,2) default 0.0,\n");
					} else if (propertys[0].equals("Date")) {
						sb.append("   " + propertys[1]
								+ " datetime comment '',\n");
					} else if ( propertys[0].equals("boolean") ) {
						sb.append("   "+propertys[1]+" int default 0,\n");
					} else if ( propertys[0].contains("List") || propertys[0].contains("ArrayList") ) {
						hasExters = true;
						subSb.append(propertys[1]+" (\n");
						List<String> subBeanPropertyList = getBeanPropertyList(bean);
					}
				}
			}
		}
		sb.append(")");
		
		sb.deleteCharAt(sb.lastIndexOf(","));
		if ( hasExters ) {
			//一对多
			
		}
		Log.i("SQLTest",sb.toString());

		return sb.toString();
	}

	private static <T> List<String> getSubClassCreateSql ( Class<T> baseCls , String subClsName ) {
		boolean type = false;
		int j = -1;
		for ( int i = 0 ; i < baseCls.getDeclaredFields().length ; i ++ ) {
			if ( baseCls.getDeclaredFields()[i].getName().equals(subClsName)) {
				j = i;
				type = true;
				break;
			} else {
				type = false;
			}
		}
		if ( type ) {
			if ( j != -1 ) {
				return getBeanPropertyList(baseCls.getDeclaredFields()[j].getType());
			}
		}
		return null;
	}
	
	public static <T> List<String> getBeanPropertyList(Class<T> bean) {
		Field[] strs = bean.getDeclaredFields();
		List<String> propertyList = new ArrayList<String>();
		for (int i = 0; i < strs.length; i++) {
			String protype = strs[i].getType().toString();
			propertyList.add(protype.substring(protype.lastIndexOf(".") + 1)
					+ "`" + strs[i].getName());
		}
		return propertyList;
	}

	/**
	 * 获取字段的get/set方法名
	 * 
	 * @author ssj
	 * 
	 * @param fieldname
	 * 
	 * @return
	 */
	private static String getMethodName(String fieldname, String type) {
		char upper = Character.toUpperCase(fieldname.charAt(0));
		return type + upper + fieldname.substring(1);
	}

	/**
	 * 获取字段值的方法
	 * 
	 * @author ssj
	 * 
	 * @param list 值数组
	 * 
	 * @return 对象数组
	 */
	public static Object[] getValues ( ArrayList list ) {
		Object[] objects = new Object[list.size()];
		int i = 0;
		for ( Object obj : list ) {
			objects[i] = obj;
			i++;
		}
		return objects;
	}

}
