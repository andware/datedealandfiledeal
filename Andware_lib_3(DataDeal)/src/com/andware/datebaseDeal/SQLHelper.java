package com.andware.datebaseDeal;

import java.util.ArrayList;
import java.util.List;
import java.util.Map;

import android.content.Context;
import android.database.Cursor;
import android.database.sqlite.SQLiteDatabase;
import android.database.sqlite.SQLiteOpenHelper;
import android.util.Log;

public class SQLHelper extends SQLiteOpenHelper{
	
	public static SQLHelper mInstance = null;
	
	private static String DB_NAME;
	
	private static int DB_VERSION = 1;

	private SQLiteDatabase db;
	
	private static ICreateTable iCreateTable;
	
	public SQLHelper(Context context,String DB_NAME) {
		// TODO Auto-generated constructor stub
		super(context, DB_NAME, null, DB_VERSION);
		this.DB_NAME = DB_NAME;
		try {
			db = getWritableDatabase();
		} catch (Exception e) {
			db=getReadableDatabase();
		}
	}
	
	/**
	 * @author ssj
	 * 
	 * 使用本数据库操作类，对象的获取必须调用该方法获取
	 * 示例：SQLHelper sqlHelper = SQLHelper.getInstance(context);
	 * 
	 * @return 返回数据库操作单例对象
	 * **/
	public static synchronized SQLHelper getInstance(Context context,String db_name,ICreateTable iCreateTableDemo) {
		
		iCreateTable = iCreateTableDemo;
		if (mInstance == null) {
			Log.i("SQLTest", "ic");
			
			mInstance = new SQLHelper(context,db_name);
		}
		return mInstance;
	}
	
	public static synchronized SQLHelper getInstance(Context context,String db_name) {
		
		if (mInstance == null) {
			
			mInstance = new SQLHelper(context,db_name);
		}
		return mInstance;
	}
	
	@Override
	public void onCreate(SQLiteDatabase arg0) {
		// TODO Auto-generated method stub
		
	}
	
	
	public void createTable () {
		if ( iCreateTable != null ) {
			Log.i("SQLTest", "ic");
			iCreateTable.createTables(db);
		}
	}

	@Override
	public void onUpgrade(SQLiteDatabase arg0, int arg1, int arg2) {
		// TODO Auto-generated method stub
		
	}
	
	public synchronized void insertObj ( Object obj , String table_name ) {
		Map map = SqlScourceDeal.getInsertSql(obj, table_name);
		String sql = (String) map.get("sql");
		ArrayList params = (ArrayList) map.get("params");
		db.execSQL(sql,SqlScourceDeal.getValues(params));
	}
	
	public synchronized void updateObj ( Object obj , String table_name ) {
		Map map = SqlScourceDeal.getUpdateSql(obj, table_name);
		String sql = (String) map.get("sql");
		ArrayList params = (ArrayList) map.get("params");
		db.execSQL(sql,SqlScourceDeal.getValues(params));
	}
	
	public synchronized <T> List<T> selectObj ( Class<T> obj , String table_name ) {
		Cursor cursor;
		
		List<T> list = null;
		
		cursor = db.query(table_name, null, null, null, null, null, null);
		
		if ( cursor == null ) {
			return null;
		} else {
			if ( cursor.moveToFirst() ) {
				list = new ArrayList<T>();
				do {
					T objT;
					objT = (T) SqlScourceDeal.setParameter2JavaBean(cursor, obj);
					
					list.add(objT);
				} while (cursor.moveToNext());
				Log.i("RoundOne","RoundLength:"+list.size());
			}
		}
		
		return list;
	}
	
	public synchronized <T> List<T> selectObj ( Class<T> obj , String table_name , String[] whereArgs , String[] selection ) {
		Cursor cursor;
		
		List<T> list = null;
		
		StringBuffer querySelection = new StringBuffer("");
		
		for ( String where : whereArgs ) {
			querySelection.append(" "+where+" = ?  and");
		}
		querySelection.delete(querySelection.lastIndexOf(" and"), querySelection.length());
		cursor = db.query(table_name, null, querySelection.toString(), selection, null, null, null);
		
		if ( cursor == null ) {
			return null;
		} else {
			if ( cursor.moveToFirst() ) {
				list = new ArrayList<T>();
				do {
					T objT;
					objT = (T) SqlScourceDeal.setParameter2JavaBean(cursor, obj);
					
					list.add(objT);
				} while (cursor.moveToNext());
				Log.i("RoundOne","RoundLength:"+list.size());
			}
		}
		
		return list;
	}
	
}
