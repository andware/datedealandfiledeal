package com.andware.FileDeal;

/**
 * 
 * @author ssj
 * 
 * 文件数据处理工具类
 * 
 * **/

import java.io.File;
import java.io.FileInputStream;
import java.io.FileNotFoundException;
import java.io.FileOutputStream;
import java.io.IOException;
import java.io.InputStream;
import java.io.ObjectInputStream;
import java.io.ObjectOutputStream;
import java.util.ArrayList;
import java.util.List;

import org.apache.http.util.EncodingUtils;

import android.R.integer;
import android.os.Environment;
import android.util.Log;

public class FileDataDeal {

	/**
	 * 生成应用文件夹
	 * 
	 * @author ssj
	 * 
	 * @param appName 应用名称
	 * @return app的文件夹路径
	 * 
	 * **/

	public static synchronized String createAppFile ( String appName ) {

		String appPath = "";

		String SDPath = Environment.getExternalStorageDirectory().toString()+"/";

		File file = new File(SDPath+appName);
		if ( !file.exists() ) {
			file.mkdir();
		}
		appPath = SDPath+appName;
		return appPath;
	}

	public static synchronized String getContentFromFile ( File file ) {
		InputStream is = null;
		String result = "";
		try {
			is = new FileInputStream(file);
			int length = is.available();
			byte[] buffer = new byte[length];
			
			is.read(buffer);
			result = EncodingUtils.getString(buffer, "UTF-8");
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				is.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
		return result;
	}
	
	/**
	 * 
	 * 复制文件 file 到 copyToPath 路径
	 * 
	 * @author ssj
	 * 
	 * @param file 需要被复制的文件 
	 * @param copyToPath 需要文件被拷贝到的路径
	 * **/

	public static synchronized void CopyFile ( File file , String copyToPath ) {
		FileInputStream fis = null;
		FileOutputStream fos = null;
		try {
			fis = new FileInputStream(file);
			byte[] buffer = new byte[fis.available()];
			fis.read(buffer);
			
			File copyFile = new File(copyToPath+"/"+file.getName());
			fos = new FileOutputStream(copyFile);
			fos.write(buffer);
			
			
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				if ( fis != null ) {
					fis.close();	
				}
				if ( fos != null ) {
					fos.close();	
				}
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

	}

	/**
	 * @author ssj
	 * 
	 * @param objects 需要保存的对象列表
	 * @param path 保存文件的路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * **/
	public static synchronized void saveObjectToFile ( List<Object> objects ,  String path ,String fileName , String Extensions  ) {
		ObjectOutputStream oos = null;
		try {
			File file = new File( path + "/" + fileName + "." + Extensions );
			oos = new ObjectOutputStream(new FileOutputStream(file));

			oos.writeInt(objects.size());
			for (Object object : objects) {
				oos.writeObject(object);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @author ssj
	 * @param <E>
	 * 
	 * @param objects 需要保存的对象列表
	 * @param path 保存文件的路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * **/
	public static synchronized <E> void saveObjectsToFile ( List<E> list ,  String path ,String fileName , String Extensions  ) {
		ObjectOutputStream oos = null;
		try {
			File file = new File( path + "/" + fileName + "." + Extensions );
			oos = new ObjectOutputStream(new FileOutputStream(file));

			oos.writeInt(list.size());
			for (E e : list) {
				oos.writeObject(e);
			}

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}

	
	
	/**
	 * @author ssj
	 * 
	 * @param object 需要保存的对象
	 * @param path 保存文件的路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * **/
	public static synchronized void saveObjectToFile ( Object object , String path ,String fileName , String Extensions ) {
		ObjectOutputStream oos = null;
		try {
			File file = new File( path + "/" + fileName + "." + Extensions );
			oos = new ObjectOutputStream(new FileOutputStream(file));

			oos.writeObject(object);

		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				oos.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}
	}
	
	/**
	 * @author ssj
	 * @param <E>
	 * @param <T>
	 * 
	 * @param path 需要获取对象链表的文件路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * @return 文件中存储的对象列表
	 * **/

	public static synchronized <E> List<E> getAllObjectsToFile( String path , String fileName , String Extensions ) {
		List<E> objects = new ArrayList<E>();
		File file = new File( path + "/" + fileName + "." + Extensions);
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(file));
			int length = ois.readInt();
			for (int i = 0; i < length; i++) {
				try {
					E e = (E) ois.readObject();
					objects.add(e);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return objects;

	}

	/**
	 * @author ssj
	 * @param <T>
	 * 
	 * @param path 需要获取对象链表的文件路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * @return 文件中存储的对象列表
	 * **/

	public static synchronized List<Object> getObjectsToFile( String path , String fileName , String Extensions ) {
		List<Object> objects = new ArrayList<Object>();
		File file = new File( path + "/" + fileName + "." + Extensions);
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(file));
			int length = ois.readInt();
			for (int i = 0; i < length; i++) {
				try {
					Object object = (Object) ois.readObject();
					objects.add(object);
				} catch (ClassNotFoundException e) {
					// TODO Auto-generated catch block
					e.printStackTrace();
				}
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return objects;

	}

	/**
	 * @author ssj
	 * 
	 * @param path 需要获取对象的文件路径
	 * @param fileName 文件名
	 * @param Extensions 文件后缀
	 * 
	 * @return 文件中存储的对象
	 * **/

	public static synchronized Object getObjectToFile( String path , String fileName , String Extensions ) {
		Object object = null;
		File file = new File( path + "/" + fileName + "." + Extensions);
		ObjectInputStream ois = null;
		try {
			ois = new ObjectInputStream(new FileInputStream(file));
			try {
				object = ois.readObject();
			} catch (ClassNotFoundException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		} catch (IOException e) {
			// TODO Auto-generated catch block
			e.printStackTrace();
		} finally {
			try {
				ois.close();
			} catch (IOException e) {
				// TODO Auto-generated catch block
				e.printStackTrace();
			}
		}

		return object;

	}
	
	

}
