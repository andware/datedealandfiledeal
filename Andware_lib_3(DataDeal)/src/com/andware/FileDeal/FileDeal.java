package com.andware.FileDeal;

import java.io.File;

public class FileDeal {

	
	/**
	 * 
	 * 判断文件是否存在
	 * 
	 * @author ssj
	 * 
	 * @param path app 路径
	 * @param fileName 文件名
	 * @param Extensions 后缀名
	 * 
	 * **/
	public static boolean IsExists ( String path ,String fileName, String Extensions ) {
		return new File( path + "/" + fileName + "." + Extensions ).exists(); 
	}
	
	public static String createFilePath ( String path ) {
		File file = new File(path);
		
		if (!file.exists()) {
			file.mkdirs();
		}
		return path;
	}
	
	/**
	 * 
	 * 判断文件是否存在
	 * 
	 * @author ssj
	 * 
	 * @param path app 路径
	 * @param fileName 文件名
	 * @param Extensions 后缀名
	 * 
	 * **/
	public static boolean IsExists ( String path) {
		return new File( path ).exists(); 
	}
	
	/**
	 * @author ssj
	 * @param path 路径
	 * 
	 * **/
	
	public static void delete(String path) {
		File file = new File(path);
        if (file.isFile()) {  
            file.delete();  
            return;  
        }  
  
        if(file.isDirectory()){  
            File[] childFiles = file.listFiles();  
            if (childFiles == null || childFiles.length == 0) {  
                file.delete();  
                return;  
            }  
      
            for (int i = 0; i < childFiles.length; i++) {  
                delete(childFiles[i].getAbsolutePath());  
            }  
            file.delete();  
        }  
    } 
}
